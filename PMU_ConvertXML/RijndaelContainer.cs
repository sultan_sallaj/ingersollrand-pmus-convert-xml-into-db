﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMU_ConvertXML
{
    public class RijndaelContainer
    {
        string plainText = "Hello, World!";    // original plaintext

        //hashAlgorithm can be "MD5" or "SHA1"
        //initVector must be 16 bytes
        //keySize can be 128, 192, 256
        string passPhrase, saltValue, hashAlgorithm, initVector;
        int passwordIterations, keySize;

        public RijndaelContainer ()
        {
            plainText = "Hello, World!";
            passPhrase = "b357l4b5";
            saltValue = "cl4mp515g0d";
            hashAlgorithm = "SHA1";
            passwordIterations = 2;
            initVector = @"6%iFu/\/\AlDor4h";
            keySize = 256;
        }

        public string Decrypt (string cipherText)
        {
            return RijndaelSimple.Decrypt
                (
                    cipherText,
                    passPhrase,
                    saltValue,
                    hashAlgorithm,
                    passwordIterations,
                    initVector,
                    keySize
                );
        }

        public string Encrypt(string plainText)
        {
            return RijndaelSimple.Encrypt
                (
                    plainText,
                    passPhrase,
                    saltValue,
                    hashAlgorithm,
                    passwordIterations,
                    initVector,
                    keySize
                );
        }
    }
}
