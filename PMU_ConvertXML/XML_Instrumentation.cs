﻿using PMU_ConvertXML.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using PMU_ConvertXML.Helpers;
using System.Globalization;

namespace PMU_ConvertXML
{
    public class XML_Instrumentation
    {
        IDictionary<string, DataTable> sourceDT = new Dictionary<string, DataTable>();
        DataTable dt_DifferentialPressure = new DataTable();
        DataTable dt_Interstage = new DataTable();
        DataTable dt_LubeOilSystem = new DataTable();
        DataTable dt_Vibration = new DataTable();
        DataTable dt_WaterManifold = new DataTable();

        DataTable dt__TypeOptions = new DataTable();

        IDictionary<string, int> dict_TypeOptions = new Dictionary<string, int>();
        IDictionary<string, int> dict_TypeOptions_LegacyMappings = new Dictionary<string, int>();

        public XML_Instrumentation (AthenaCPQEntities context)
        {
            _fillTypeOptions(context);

            dt_DifferentialPressure = context.Instrumentation_DifferentialPressure.CopySchemaToDataTable();
            dt_Interstage = context.Instrumentation_Interstage.CopySchemaToDataTable();
            dt_LubeOilSystem = context.Instrumentation_LubeOilSystem.CopySchemaToDataTable();
            dt_Vibration = context.Instrumentation_Vibration.CopySchemaToDataTable();
            dt_WaterManifold = context.Instrumentation_WaterManifold.CopySchemaToDataTable();

            _flatten(context);

            sourceDT.Add("PMU.Instrumentation_DifferentialPressure", dt_DifferentialPressure);
            sourceDT.Add("PMU.Instrumentation_Interstage", dt_Interstage);
            sourceDT.Add("PMU.Instrumentation_LubeOilSystem", dt_LubeOilSystem);
            sourceDT.Add("PMU.Instrumentation_Vibration", dt_Vibration);
            sourceDT.Add("PMU.Instrumentation_WaterManifold", dt_WaterManifold);

            // Copy the DataTable to SQL Server
            PMU_Operations.dump(sourceDT);
        }

        private void _fillTypeOptions(AthenaCPQEntities context)
        {
            Console.Write("-Handling Type Options and mapping to legacy type selection values");
            Console.WriteLine();
            //in case typeoptions table is empty, fill it up
            if (context.Instrumentation__TypeOptions.Count() == 0)
            {
                dt__TypeOptions = context.Instrumentation__TypeOptions.CopySchemaToDataTable();
                _populateTypeOptions();
                PMU_Operations.dump("PMU.Instrumentation__TypeOptions", dt__TypeOptions);
            }

            dict_TypeOptions = (from options in context.Instrumentation__TypeOptions
                                select new
                                {
                                    options.Type,
                                    options.ID
                                }
                    ).ToDictionary(x => x.Type, y => y.ID);

            _typeOptionsLegacyMappings(dict_TypeOptions);
        }

        private void _flatten(AthenaCPQEntities context)
        {
            Console.Write("-Parsing XML document and preparing data");
            Console.WriteLine();
            var oldPMUInstrumentation = (from pmu in context.Mappings.AsNoTracking()
                                         where !context.Instrumentation_Interstage.Any(x => (x.HeaderID == pmu.ID))
                                         select new pmuXMLDocDTO
                                         {
                                             PMUID = pmu.ID,
                                             XMLDoc = pmu.pmuStrXMLInstrumentation,
                                         });

            var count = oldPMUInstrumentation.Count();
            var index = 1;
            foreach (var details in oldPMUInstrumentation)
            {

                _xmlMapping(details);
                Console.Write("\r        " + index++ + " out of " + count + " records..");
            }
            Console.WriteLine();
        }

        private void _typeOptionsLegacyMappings(IDictionary<string, int> dict)
        {
            IDictionary<string, int> _dict = new Dictionary<string, int>();
            foreach (KeyValuePair<string, int> detail in dict)
            {
                switch (detail.Key)
                {
                    case "Gauge":
                        _dict.Add("rbInterStageTempLocalGuage", detail.Value);
                        _dict.Add("rbInterStagePressLocalGuage", detail.Value);
                        _dict.Add("rbLosTempLocGuage", detail.Value);
                        _dict.Add("rbLosPressLocGuage", detail.Value);
                        _dict.Add("rbWMTempLocGuage", detail.Value);
                        _dict.Add("rbWMPressLocGuage", detail.Value);
                        _dict.Add("rbDPPressLocGuage", detail.Value);
                        break;
                    case "GaugeWell":
                        _dict.Add("rbInterStateTempLocalGuageWell", detail.Value);
                        _dict.Add("rbLosTempLocGuageWithWell", detail.Value);
                        _dict.Add("rbLosPressLocGuagewithWell", detail.Value);
                        _dict.Add("rbWMTempLocGuageWithWell", detail.Value);
                        _dict.Add("rbWMPressLocGuageWithWell", detail.Value);
                        _dict.Add("rbDPPressLocGuagewithWell", detail.Value);
                        break;
                    case "GaugeBB":
                        _dict.Add("rbInterStagePressLocalGuagewWell", detail.Value);
                        break;
                    case "RTD":
                        _dict.Add("rbInterStageTempOnPanelRTD", detail.Value);
                        _dict.Add("rbLosTempOnPanelRTD", detail.Value);
                        _dict.Add("rbTempOnPanelRTD", detail.Value);
                        break;
                    case "RTDWell":
                        _dict.Add("rbInterStageTempOnPanelRTDwell", detail.Value);
                        _dict.Add("rbLosTempOnPanelRTDWithWell", detail.Value);
                        _dict.Add("rbTempOnPanelRTDWithWell", detail.Value);
                        break;
                    case "RTDDualWell":
                        _dict.Add("rbInterStageTempOnPanelRTDdualWell", detail.Value);
                        _dict.Add("rbLosTempOnPanelDualRTDwithWell", detail.Value);
                        _dict.Add("rbTempOnPanelDualRTDWithWell", detail.Value);
                        break;
                    case "Setra":
                        _dict.Add("rbInterStagePOPSetra", detail.Value);
                        _dict.Add("rbLospressureOnPanelSetra", detail.Value);
                        _dict.Add("rbWMPressureOnPanelSetra", detail.Value);
                        _dict.Add("rbDPPressOnPanelSetra", detail.Value);
                        break;
                    case "Setra-BB":
                        _dict.Add("rbInterStagePOPSetra-BB", detail.Value);
                        _dict.Add("rbLospressureOnPanelSetra-BB", detail.Value);
                        _dict.Add("rbWMPressureOnPanelSetra-BB", detail.Value);
                        _dict.Add("rbDPPressOnPanelSetra-BB", detail.Value);
                        break;
                    case "Setra-M":
                        _dict.Add("rbDPPressOnPanelSetra-M", detail.Value);
                        break;
                    case "Setra-BB-M":
                        _dict.Add("rbDPPressOnPanelSetra-BB-M", detail.Value);
                        break;
                    case "Rose2088":
                        _dict.Add("rbInterStagePOPRose2088", detail.Value);
                        _dict.Add("rbLosPressureOnPanelRose2088", detail.Value);
                        _dict.Add("rbWMPressureOnPanelRose2088", detail.Value);
                        _dict.Add("rbDPPressOnPanelRose2088", detail.Value);
                        break;
                    case "Rose2088-BB":
                        _dict.Add("rbInterStagePOPRose2088-BB", detail.Value);
                        _dict.Add("rbLosPressureOnPanelRose2088-BB", detail.Value);
                        _dict.Add("rbWMPressureOnPanelRose2088-BB", detail.Value);
                        _dict.Add("rbDPPressOnPanelRose2088-BB", detail.Value);
                        break;
                    case "Rose2088-M":
                        _dict.Add("rbDPPressOnPanelRose2088-M", detail.Value);
                        break;
                    case "Rose2088-BB-M":
                        _dict.Add("rbDPPressOnPanelRose2088-BB-M", detail.Value);
                        break;
                    case "Rose3051":
                        _dict.Add("rbInterStagePOPRTDdualWell", detail.Value);
                        _dict.Add("rbLosPressureOnpanelRose3051", detail.Value);
                        _dict.Add("rbWMRose3051", detail.Value);
                        _dict.Add("rbDPPressOnPanelRose3051", detail.Value);
                        break;
                    case "Rose3051-BB":
                        _dict.Add("rbInterStagePOPRTDdualWell-BB", detail.Value);
                        _dict.Add("rbLosPressureOnpanelRose3051-BB", detail.Value);
                        _dict.Add("rbWMRose3051-BB", detail.Value);
                        _dict.Add("rbDPPressOnPanelRose3051-BB", detail.Value);
                        break;
                    case "Rose3051-M":
                        _dict.Add("rbDPPressOnPanelRose3051-M", detail.Value);
                        break;
                    case "Rose3051-BB-M":
                        _dict.Add("rbDPPressOnPanelRose3051-BB-M", detail.Value);
                        break;
                    case "Rose3051LCD":
                        _dict.Add("rbInterStagePOPRTDRose3051LCD", detail.Value);
                        _dict.Add("rbLosPressureOnPanelRose3051WLCD", detail.Value);
                        _dict.Add("rbWMPressureOnPanelRose3051WithLCD", detail.Value);
                        _dict.Add("rbDPPressOnPanelRose3051WithLCD", detail.Value);
                        break;
                    case "Rose3051LCD-BB":
                        _dict.Add("rbInterStagePOPRTDRose3051LCD-BB", detail.Value);
                        _dict.Add("rbLosPressureOnPanelRose3051WLCD-BB", detail.Value);
                        _dict.Add("rbWMPressureOnPanelRose3051WithLCD-BB", detail.Value);
                        _dict.Add("rbDPPressOnPanelRose3051WithLCD-BB", detail.Value);
                        break;
                    case "Rose3051LCD-M":
                        _dict.Add("rbDPPressOnPanelRose3051WithLCD-M", detail.Value);
                        break;
                    case "Rose3051LCD-BB-M":
                        _dict.Add("rbDPPressOnPanelRose3051WithLCD-BB-M", detail.Value);
                        break;
                    case "XOnlyShinkawa":
                        _dict.Add("rbVibrationTempOnPanelRTD", detail.Value);
                        _dict.Add("rbVibOnlyShinkawa", detail.Value);
                        _dict.Add("rbVibrationAxialOnlyShinkawa", detail.Value);
                        break;
                    case "XOnlyBentlyNevada":
                        _dict.Add("rbVibrationTempOnPanelTempOnPanelWithWell", detail.Value);
                        _dict.Add("rbVibrationBentlyOnly", detail.Value);
                        _dict.Add("rbVibrationAxialOnlyBentlyNevada", detail.Value);
                        break;
                    case "XYBentlyNevada":
                        _dict.Add("rbVibrationTempOnPanelDualRTDWithWell", detail.Value);
                        _dict.Add("rbVibrationAxialXYBentlyNevada", detail.Value);
                        break;
                }
            }

            dict_TypeOptions_LegacyMappings = _dict;
        }

        private void _populateTypeOptions ()
        {
            DataRow workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Gauge";
            workRow["Description"] = "Gauge";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "GaugeWell";
            workRow["Description"] = "Gauge w/Well";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "GaugeBB";
            workRow["Description"] = "Gauge w/Block and Bleed";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "RTD";
            workRow["Description"] = "RTD";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "RTDWell";
            workRow["Description"] = "RTD w/Well";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "RTDDualWell";
            workRow["Description"] = "Dual RTD w/Well";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Setra";
            workRow["Description"] = "Setra";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Setra-BB";
            workRow["Description"] = "Setra w/Block and Bleed";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Setra-M";
            workRow["Description"] = "Setra w/Manifold";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Setra-BB-M";
            workRow["Description"] = "Setra w/(Block and Bleed, Manifold)";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Rose2088";
            workRow["Description"] = "Rosemount 2088";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Rose2088-BB";
            workRow["Description"] = "Rosemount 2088 w/Block and Bleed";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Rose2088-M";
            workRow["Description"] = "Rosemount 2088 w/Manifold";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Rose2088-BB-M";
            workRow["Description"] = "Rosemount 2088 w/(Block and Bleed, Manifold)";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Rose3051";
            workRow["Description"] = "Rosemount 3051";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Rose3051-BB";
            workRow["Description"] = "Rosemount 3051 w/Block and Bleed";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Rose3051-M";
            workRow["Description"] = "Rosemount 3051 w/Manifold";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Rose3051-BB-M";
            workRow["Description"] = "Rosemount 3051 w/(Block and Bleed, Manifold)";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Rose3051LCD";
            workRow["Description"] = "Rosemount 3051 w/LCD";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Rose3051LCD-BB";
            workRow["Description"] = "Rosemount 3051 w/LCD w/Block and Bleed ";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Rose3051LCD-M";
            workRow["Description"] = "Rosemount 3051 w/(LCD, Manifold)";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "Rose3051LCD-BB-M";
            workRow["Description"] = "Rosemount 3051 w/(LCD, Block and Bleed, Manifold)";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "XOnlyShinkawa";
            workRow["Description"] = "X Only Shinkawa";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "XOnlyBentlyNevada";
            workRow["Description"] = "X Only Bently Nevada";
            dt__TypeOptions.Rows.Add(workRow);
            workRow = dt__TypeOptions.NewRow();
            workRow["Type"] = "XYBentlyNevada";
            workRow["Description"] = "X-Y Bently Nevada";
            dt__TypeOptions.Rows.Add(workRow);
        }

        private void _xmlMapping(pmuXMLDocDTO details)
        {
            var xDoc = XDocument.Parse(new RijndaelContainer().Decrypt(details.XMLDoc));
            var xmlNode = xDoc.Descendants("Instrumentation").First();

            DataRow ii_WorkRow = dt_Interstage.NewRow();
            var II_Options = bool.Parse(xmlNode.Element("II_BlockAndBleed").Value) ? "-BB" : "";

            ii_WorkRow["HeaderID"] = details.PMUID;
            ii_WorkRow["TemperatureLocal_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("II_RadioTempLocal").Value];
            ii_WorkRow["TemperatureLocal_Stage1_Inlet"] = bool.Parse(xmlNode.Element("II_TempLocal_Stage1_In").Value);
            ii_WorkRow["TemperatureLocal_Stage1_Outlet"] = bool.Parse(xmlNode.Element("II_TempLocal_Stage1_Out").Value);
            ii_WorkRow["TemperatureLocal_Stage2_Inlet"] = bool.Parse(xmlNode.Element("II_TempLocal_Stage2_In").Value);
            ii_WorkRow["TemperatureLocal_Stage2_Outlet"] = bool.Parse(xmlNode.Element("II_TempLocal_Stage2_Out").Value);
            ii_WorkRow["TemperatureLocal_Stage3_Inlet"] = bool.Parse(xmlNode.Element("II_TempLocal_Stage3_In").Value);
            ii_WorkRow["TemperatureLocal_Stage3_Outlet"] = bool.Parse(xmlNode.Element("II_TempLocal_Stage3_Out").Value);
            ii_WorkRow["TemperatureLocal_Stage4_Inlet"] = bool.Parse(xmlNode.Element("II_TempLocal_Stage4_In").Value);
            ii_WorkRow["TemperatureLocal_Stage4_Outlet"] = bool.Parse(xmlNode.Element("II_TempLocal_Stage4_Out").Value);
            ii_WorkRow["TemperatureLocal_CompressDischarge"] = bool.Parse(xmlNode.Element("II_TempLocal_CompDist").Value);
            ii_WorkRow["TemperatureLocal_System"] = bool.Parse(xmlNode.Element("II_TempLocal_System").Value);
            
            ii_WorkRow["TemperaturePanel_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("II_RadioTempPanel").Value];
            ii_WorkRow["TemperaturePanel_Stage1_Inlet"] = bool.Parse(xmlNode.Element("II_TempPanel_Stage1_In").Value);
            ii_WorkRow["TemperaturePanel_Stage1_Outlet"] = bool.Parse(xmlNode.Element("II_TempPanel_Stage1_Out").Value);
            ii_WorkRow["TemperaturePanel_Stage2_Inlet"] = bool.Parse(xmlNode.Element("II_TempPanel_Stage2_In").Value);
            ii_WorkRow["TemperaturePanel_Stage2_Outlet"] = bool.Parse(xmlNode.Element("II_TempPanel_Stage2_Out").Value);
            ii_WorkRow["TemperaturePanel_Stage3_Inlet"] = bool.Parse(xmlNode.Element("II_TempPanel_Stage3_In").Value);
            ii_WorkRow["TemperaturePanel_Stage3_Outlet"] = bool.Parse(xmlNode.Element("II_TempPanel_Stage3_Out").Value);
            ii_WorkRow["TemperaturePanel_Stage4_Inlet"] = bool.Parse(xmlNode.Element("II_TempPanel_Stage4_In").Value);
            ii_WorkRow["TemperaturePanel_Stage4_Outlet"] = bool.Parse(xmlNode.Element("II_TempPanel_Stage4_Out").Value);
            ii_WorkRow["TemperaturePanel_CompressDischarge"] = bool.Parse(xmlNode.Element("II_TempPanel_CompDist").Value);
            ii_WorkRow["TemperaturePanel_System"] = bool.Parse(xmlNode.Element("II_TempPanel_System").Value);
            
            ii_WorkRow["PressureLocal_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("II_RadioPressLocal").Value];
            ii_WorkRow["PressureLocal_Stage1_Inlet"] = bool.Parse(xmlNode.Element("II_PressLocal_Stage1_In").Value);
            ii_WorkRow["PressureLocal_Stage1_Outlet"] = bool.Parse(xmlNode.Element("II_PressLocal_Stage1_Out").Value);
            ii_WorkRow["PressureLocal_Stage2_Inlet"] = bool.Parse(xmlNode.Element("II_PressLocal_Stage2_In").Value);
            ii_WorkRow["PressureLocal_Stage2_Outlet"] = bool.Parse(xmlNode.Element("II_PressLocal_Stage2_Out").Value);
            ii_WorkRow["PressureLocal_Stage3_Inlet"] = bool.Parse(xmlNode.Element("II_PressLocal_Stage3_In").Value);
            ii_WorkRow["PressureLocal_Stage3_Outlet"] = bool.Parse(xmlNode.Element("II_PressLocal_Stage3_Out").Value);
            ii_WorkRow["PressureLocal_Stage4_Inlet"] = bool.Parse(xmlNode.Element("II_PressLocal_Stage4_In").Value);
            ii_WorkRow["PressureLocal_Stage4_Outlet"] = bool.Parse(xmlNode.Element("II_PressLocal_Stage4_Out").Value);
            ii_WorkRow["PressureLocal_CompressDischarge"] = bool.Parse(xmlNode.Element("II_PressLocal_CompDist").Value);
            ii_WorkRow["PressureLocal_System"] = bool.Parse(xmlNode.Element("II_PressLocal_System").Value);
            
            ii_WorkRow["PressurePanel_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("II_RadioPressPanel").Value + II_Options];
            ii_WorkRow["PressurePanel_Stage1_Inlet"] = bool.Parse(xmlNode.Element("II_PressPanel_Stage1_In").Value);
            ii_WorkRow["PressurePanel_Stage1_Outlet"] = bool.Parse(xmlNode.Element("II_PressPanel_Stage1_Out").Value);
            ii_WorkRow["PressurePanel_Stage2_Inlet"] = bool.Parse(xmlNode.Element("II_PressPanel_Stage2_In").Value);
            ii_WorkRow["PressurePanel_Stage2_Outlet"] = bool.Parse(xmlNode.Element("II_PressPanel_Stage2_Out").Value);
            ii_WorkRow["PressurePanel_Stage3_Inlet"] = bool.Parse(xmlNode.Element("II_PressPanel_Stage3_In").Value);
            ii_WorkRow["PressurePanel_Stage3_Outlet"] = bool.Parse(xmlNode.Element("II_PressPanel_Stage3_Out").Value);
            ii_WorkRow["PressurePanel_Stage4_Inlet"] = bool.Parse(xmlNode.Element("II_PressPanel_Stage4_In").Value);
            ii_WorkRow["PressurePanel_Stage4_Outlet"] = bool.Parse(xmlNode.Element("II_PressPanel_Stage4_Out").Value);
            ii_WorkRow["PressurePanel_CompressDischarge"] = bool.Parse(xmlNode.Element("II_PressPanel_CompDist").Value);
            ii_WorkRow["PressurePanel_System"] = bool.Parse(xmlNode.Element("II_PressPanel_System").Value);
            ii_WorkRow["Sell"] = int.Parse(xmlNode.Element("II_Sell").Value, NumberStyles.AllowThousands);
            ii_WorkRow["List"] = int.Parse(xmlNode.Element("II_List").Value, NumberStyles.AllowThousands);
            ii_WorkRow["Base"] = int.Parse(xmlNode.Element("II_Base").Value, NumberStyles.AllowThousands);
            ii_WorkRow["CountRTD"] = int.Parse(xmlNode.Element("II_RTDCount").Value, NumberStyles.AllowThousands);
            ii_WorkRow["Count420"] = int.Parse(xmlNode.Element("II_420Count").Value, NumberStyles.AllowThousands);
            dt_Interstage.Rows.Add(ii_WorkRow);

            DataRow ls_WorkRow = dt_LubeOilSystem.NewRow();
            var LS_Options = bool.Parse(xmlNode.Element("LS_BlockAndBleed").Value) ? "-BB" : "";
            ls_WorkRow["HeaderID"] = details.PMUID;
            ls_WorkRow["TemperatureLocal_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("LS_RadioTempLocal").Value];
            ls_WorkRow["TemperatureLocal_OilTank"] = bool.Parse(xmlNode.Element("LS_TempLocal_OilTank").Value);
            ls_WorkRow["TemperatureLocal_AfterOilTank"] = bool.Parse(xmlNode.Element("LS_TempLocal_AfterOilTank").Value);
            ls_WorkRow["TemperatureLocal_AfterOilFilter"] = bool.Parse(xmlNode.Element("LS_TempLocal_AfterOilFilter").Value);
            ls_WorkRow["TemperatureLocal_ForcedLube"] = bool.Parse(xmlNode.Element("LS_TempLocal_ForcedLube").Value);
            
            ls_WorkRow["TemperaturePanel_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("LS_RadioTempPanel").Value];
            ls_WorkRow["TemperaturePanel_OilTank"] = bool.Parse(xmlNode.Element("LS_TempPanel_OilTank").Value);
            ls_WorkRow["TemperaturePanel_AfterOilTank"] = bool.Parse(xmlNode.Element("LS_TempPanel_AfterOilTank").Value);
            ls_WorkRow["TemperaturePanel_AfterOilFilter"] = bool.Parse(xmlNode.Element("LS_TempPanel_AfterOilFilter").Value);
            ls_WorkRow["TemperaturePanel_BearingDrain"] = bool.Parse(xmlNode.Element("LS_TempPanel_BearingDrain").Value);
            ls_WorkRow["TemperaturePanel_ForcedLube"] = bool.Parse(xmlNode.Element("LS_TempPanel_ForcedLube").Value);
            
            ls_WorkRow["PressureLocal_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("LS_RadioPressLocal").Value];
            ls_WorkRow["PressureLocal_OilTank"] = bool.Parse(xmlNode.Element("LS_PressLocal_OilTank").Value);
            ls_WorkRow["PressureLocal_AfterOilTank"] = bool.Parse(xmlNode.Element("LS_PressLocal_AfterOilTank").Value);
            ls_WorkRow["PressureLocal_AfterOilFilter"] = bool.Parse(xmlNode.Element("LS_PressLocal_AfterOilFilter").Value);
            ls_WorkRow["PressureLocal_ForcedLube"] = bool.Parse(xmlNode.Element("LS_PressLocal_ForcedLube").Value);
            
            ls_WorkRow["PressurePanel_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("LS_RadioPressPanel").Value + LS_Options];
            ls_WorkRow["PressurePanel_OilTank"] = bool.Parse(xmlNode.Element("LS_PressPanel_OilTank").Value);
            ls_WorkRow["PressurePanel_AfterOilTank"] = bool.Parse(xmlNode.Element("LS_PressPanel_AfterOilTank").Value);
            ls_WorkRow["PressurePanel_AfterOilFilter"] = bool.Parse(xmlNode.Element("LS_PressPanel_AfterOilFilter").Value);
            ls_WorkRow["PressurePanel_BearingDrain"] = bool.Parse(xmlNode.Element("LS_PressPanel_BearingDrain").Value);
            ls_WorkRow["PressurePanel_ForcedLube"] = bool.Parse(xmlNode.Element("LS_PressPanel_ForcedLube").Value);
            ls_WorkRow["Sell"] = int.Parse(xmlNode.Element("LS_Sell").Value, NumberStyles.AllowThousands);
            ls_WorkRow["List"] = int.Parse(xmlNode.Element("LS_List").Value, NumberStyles.AllowThousands);
            ls_WorkRow["Base"] = int.Parse(xmlNode.Element("LS_Base").Value, NumberStyles.AllowThousands);
            ls_WorkRow["CountRTD"] = int.Parse(xmlNode.Element("LS_RTDCount").Value, NumberStyles.AllowThousands);
            ls_WorkRow["Count420"] = int.Parse(xmlNode.Element("LS_420Count").Value, NumberStyles.AllowThousands);
            dt_LubeOilSystem.Rows.Add(ls_WorkRow);

            DataRow wm_WorkRow = dt_WaterManifold.NewRow();
            var WM_Options = bool.Parse(xmlNode.Element("WM_BlockAndBleed").Value) ? "-BB" : "";

            wm_WorkRow["HeaderID"] = details.PMUID;
            wm_WorkRow["TemperatureLocal_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("WM_RadioTempLocal").Value];
            wm_WorkRow["TemperatureLocal_Stage1_Inlet"] = bool.Parse(xmlNode.Element("WM_TempLocal_Stage1_In").Value);
            wm_WorkRow["TemperatureLocal_Stage1_Outlet"] = bool.Parse(xmlNode.Element("WM_TempLocal_Stage1_Out").Value);
            wm_WorkRow["TemperatureLocal_Stage2_Inlet"] = bool.Parse(xmlNode.Element("WM_TempLocal_Stage2_In").Value);
            wm_WorkRow["TemperatureLocal_Stage2_Outlet"] = bool.Parse(xmlNode.Element("WM_TempLocal_Stage2_Out").Value);
            wm_WorkRow["TemperatureLocal_Stage3_Inlet"] = bool.Parse(xmlNode.Element("WM_TempLocal_Stage3_In").Value);
            wm_WorkRow["TemperatureLocal_Stage3_Outlet"] = bool.Parse(xmlNode.Element("WM_TempLocal_Stage3_Out").Value);
            wm_WorkRow["TemperatureLocal_Stage4_Inlet"] = bool.Parse(xmlNode.Element("WM_TempLocal_Stage4_In").Value);
            wm_WorkRow["TemperatureLocal_Stage4_Outlet"] = bool.Parse(xmlNode.Element("WM_TempLocal_Stage4_Out").Value);
            wm_WorkRow["TemperatureLocal_ManifoldHeader_Inlet"] = bool.Parse(xmlNode.Element("WM_TempLocal_ManifoldHead_In").Value);
            wm_WorkRow["TemperatureLocal_ManifoldHeader_Outlet"] = bool.Parse(xmlNode.Element("WM_TempLocal_ManifoldHead_Out").Value);
            wm_WorkRow["TemperatureLocal_OilCooler_Inlet"] = bool.Parse(xmlNode.Element("WM_TempLocal_OilCooler_In").Value);
            wm_WorkRow["TemperatureLocal_OilCooler_Outlet"] = bool.Parse(xmlNode.Element("WM_TempLocal_OilCooler_Out").Value);

            wm_WorkRow["TemperaturePanel_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("WM_RadioTempPanel").Value];
            wm_WorkRow["TemperaturePanel_Stage1_Inlet"] = bool.Parse(xmlNode.Element("WM_TempPanel_Stage1_In").Value);
            wm_WorkRow["TemperaturePanel_Stage1_Outlet"] = bool.Parse(xmlNode.Element("WM_TempPanel_Stage1_Out").Value);
            wm_WorkRow["TemperaturePanel_Stage2_Inlet"] = bool.Parse(xmlNode.Element("WM_TempPanel_Stage2_In").Value);
            wm_WorkRow["TemperaturePanel_Stage2_Outlet"] = bool.Parse(xmlNode.Element("WM_TempPanel_Stage2_Out").Value);
            wm_WorkRow["TemperaturePanel_Stage3_Inlet"] = bool.Parse(xmlNode.Element("WM_TempPanel_Stage3_In").Value);
            wm_WorkRow["TemperaturePanel_Stage3_Outlet"] = bool.Parse(xmlNode.Element("WM_TempPanel_Stage3_Out").Value);
            wm_WorkRow["TemperaturePanel_Stage4_Inlet"] = bool.Parse(xmlNode.Element("WM_TempPanel_Stage4_In").Value);
            wm_WorkRow["TemperaturePanel_Stage4_Outlet"] = bool.Parse(xmlNode.Element("WM_TempPanel_Stage4_Out").Value);
            wm_WorkRow["TemperaturePanel_ManifoldHeader_Inlet"] = bool.Parse(xmlNode.Element("WM_TempPanel_ManifoldHead_In").Value);
            wm_WorkRow["TemperaturePanel_ManifoldHeader_Outlet"] = bool.Parse(xmlNode.Element("WM_TempPanel_ManifoldHead_Out").Value);
            wm_WorkRow["TemperaturePanel_OilCooler_Inlet"] = bool.Parse(xmlNode.Element("WM_TempPanel_OilCooler_In").Value);
            wm_WorkRow["TemperaturePanel_OilCooler_Outlet"] = bool.Parse(xmlNode.Element("WM_TempPanel_OilCooler_Out").Value);

            wm_WorkRow["PressureLocal_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("WM_RadioPressLocal").Value];
            wm_WorkRow["PressureLocal_Stage1_Inlet"] = bool.Parse(xmlNode.Element("WM_PressLocal_Stage1_In").Value);
            wm_WorkRow["PressureLocal_Stage1_Outlet"] = bool.Parse(xmlNode.Element("WM_PressLocal_Stage1_Out").Value);
            wm_WorkRow["PressureLocal_Stage2_Inlet"] = bool.Parse(xmlNode.Element("WM_PressLocal_Stage2_In").Value);
            wm_WorkRow["PressureLocal_Stage2_Outlet"] = bool.Parse(xmlNode.Element("WM_PressLocal_Stage2_Out").Value);
            wm_WorkRow["PressureLocal_Stage3_Inlet"] = bool.Parse(xmlNode.Element("WM_PressLocal_Stage3_In").Value);
            wm_WorkRow["PressureLocal_Stage3_Outlet"] = bool.Parse(xmlNode.Element("WM_PressLocal_Stage3_Out").Value);
            wm_WorkRow["PressureLocal_Stage4_Inlet"] = bool.Parse(xmlNode.Element("WM_PressLocal_Stage4_In").Value);
            wm_WorkRow["PressureLocal_Stage4_Outlet"] = bool.Parse(xmlNode.Element("WM_PressLocal_Stage4_Out").Value);
            wm_WorkRow["PressureLocal_ManifoldHeader_Inlet"] = bool.Parse(xmlNode.Element("WM_PressLocal_ManifoldHead_In").Value);
            wm_WorkRow["PressureLocal_ManifoldHeader_Outlet"] = bool.Parse(xmlNode.Element("WM_PressLocal_ManifoldHead_Out").Value);
            wm_WorkRow["PressureLocal_OilCooler_Inlet"] = bool.Parse(xmlNode.Element("WM_PressLocal_OilCooler_In").Value);
            wm_WorkRow["PressureLocal_OilCooler_Outlet"] = bool.Parse(xmlNode.Element("WM_PressLocal_OilCooler_Out").Value);

            wm_WorkRow["PressurePanel_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("WM_RadioPressPanel").Value + WM_Options];
            wm_WorkRow["PressurePanel_Stage1_Inlet"] = bool.Parse(xmlNode.Element("WM_PressPanel_Stage1_In").Value);
            wm_WorkRow["PressurePanel_Stage1_Outlet"] = bool.Parse(xmlNode.Element("WM_PressPanel_Stage1_Out").Value);
            wm_WorkRow["PressurePanel_Stage2_Inlet"] = bool.Parse(xmlNode.Element("WM_PressPanel_Stage2_In").Value);
            wm_WorkRow["PressurePanel_Stage2_Outlet"] = bool.Parse(xmlNode.Element("WM_PressPanel_Stage2_Out").Value);
            wm_WorkRow["PressurePanel_Stage3_Inlet"] = bool.Parse(xmlNode.Element("WM_PressPanel_Stage3_In").Value);
            wm_WorkRow["PressurePanel_Stage3_Outlet"] = bool.Parse(xmlNode.Element("WM_PressPanel_Stage3_Out").Value);
            wm_WorkRow["PressurePanel_Stage4_Inlet"] = bool.Parse(xmlNode.Element("WM_PressPanel_Stage4_In").Value);
            wm_WorkRow["PressurePanel_Stage4_Outlet"] = bool.Parse(xmlNode.Element("WM_PressPanel_Stage4_Out").Value);
            wm_WorkRow["PressurePanel_ManifoldHeader_Inlet"] = bool.Parse(xmlNode.Element("WM_PressPanel_ManifoldHead_In").Value);
            wm_WorkRow["PressurePanel_ManifoldHeader_Outlet"] = bool.Parse(xmlNode.Element("WM_PressPanel_ManifoldHead_Out").Value);
            wm_WorkRow["PressurePanel_OilCooler_Inlet"] = bool.Parse(xmlNode.Element("WM_PressPanel_OilCooler_In").Value);
            wm_WorkRow["PressurePanel_OilCooler_Outlet"] = bool.Parse(xmlNode.Element("WM_PressPanel_OilCooler_Out").Value);
            wm_WorkRow["Sell"] = int.Parse(xmlNode.Element("WM_Sell").Value, NumberStyles.AllowThousands);
            wm_WorkRow["List"] = int.Parse(xmlNode.Element("WM_List").Value, NumberStyles.AllowThousands);
            wm_WorkRow["Base"] = int.Parse(xmlNode.Element("WM_Base").Value, NumberStyles.AllowThousands);
            wm_WorkRow["CountRTD"] = int.Parse(xmlNode.Element("WM_RTDCount").Value, NumberStyles.AllowThousands);
            wm_WorkRow["Count420"] = int.Parse(xmlNode.Element("WM_420Count").Value, NumberStyles.AllowThousands);
            dt_WaterManifold.Rows.Add(wm_WorkRow);

            DataRow dp_WorkRow = dt_DifferentialPressure.NewRow();
            var DP_Options = bool.Parse(xmlNode.Element("DP_BlockAndBleed").Value) ? "-BB" : "";
            DP_Options = DP_Options +( bool.Parse(xmlNode.Element("DP_WithManifold").Value) ? "-M" : "");

            dp_WorkRow["HeaderID"] = details.PMUID;
            dp_WorkRow["PressureLocal_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("DP_RadioPressLocal").Value];

            dp_WorkRow["PressureLocal_InletFilter"] = bool.Parse(xmlNode.Element("DP_PressLocal_InletFilter").Value);
            dp_WorkRow["PressureLocal_OilFilter"] = bool.Parse(xmlNode.Element("DP_PressLocal_OilFilter").Value);
            dp_WorkRow["PressureLocal_Stage1"] = bool.Parse(xmlNode.Element("DP_PressLocal_Stage1").Value);
            dp_WorkRow["PressureLocal_Stage2"] = bool.Parse(xmlNode.Element("DP_PressLocal_Stage2").Value);
            dp_WorkRow["PressureLocal_Stage3"] = bool.Parse(xmlNode.Element("DP_PressLocal_Stage3").Value);

            dp_WorkRow["PressurePanel_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("DP_RadioPressPanel").Value + DP_Options];
            dp_WorkRow["PressurePanel_InletFilter"] = bool.Parse(xmlNode.Element("DP_PressPanel_InletFilter").Value);
            dp_WorkRow["PressurePanel_OilFilter"] = bool.Parse(xmlNode.Element("DP_PressPanel_OilFilter").Value);
            dp_WorkRow["PressurePanel_Stage1"] = bool.Parse(xmlNode.Element("DP_PressPanel_Stage1").Value);
            dp_WorkRow["PressurePanel_Stage2"] = bool.Parse(xmlNode.Element("DP_PressPanel_Stage2").Value);
            dp_WorkRow["PressurePanel_Stage3"] = bool.Parse(xmlNode.Element("DP_PressPanel_Stage3").Value);
            dp_WorkRow["Sell"] = int.Parse(xmlNode.Element("DP_Sell").Value, NumberStyles.AllowThousands);
            dp_WorkRow["List"] = int.Parse(xmlNode.Element("DP_List").Value, NumberStyles.AllowThousands);
            dp_WorkRow["Base"] = int.Parse(xmlNode.Element("DP_Base").Value, NumberStyles.AllowThousands);
            dp_WorkRow["Count420"] = int.Parse(xmlNode.Element("DP_420Count").Value, NumberStyles.AllowThousands);
            dt_DifferentialPressure.Rows.Add(dp_WorkRow);

            DataRow v_WorkRow = dt_Vibration.NewRow();

            v_WorkRow["HeaderID"] = details.PMUID;
            v_WorkRow["Vibration_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("VB_Radio").Value];
            v_WorkRow["Vibration_Stage1"] = bool.Parse(xmlNode.Element("VB_Stage1").Value);
            v_WorkRow["Vibration_Stage2"] = bool.Parse(xmlNode.Element("VB_Stage2").Value);
            v_WorkRow["Vibration_Stage3"] = bool.Parse(xmlNode.Element("VB_Stage3").Value);
            v_WorkRow["Vibration_Stage4"] = bool.Parse(xmlNode.Element("VB_Stage4").Value);
            v_WorkRow["Vibration_Bulgear"] = bool.Parse(xmlNode.Element("VB_Bulgear").Value);

            v_WorkRow["Keyphasor_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("KP_Radio").Value];
            v_WorkRow["Keyphasor_Stage1"] = bool.Parse(xmlNode.Element("KP_Stage1").Value);
            v_WorkRow["Keyphasor_Stage3"] = bool.Parse(xmlNode.Element("KP_Stage3").Value);

            v_WorkRow["AxialVibrationProbe_TypeID"] = dict_TypeOptions_LegacyMappings[xmlNode.Element("AX_Radio").Value];
            v_WorkRow["AxialVibrationProbe_Bulgear"] = bool.Parse(xmlNode.Element("AX_Bulgear").Value);
            v_WorkRow["Sell"] = int.Parse(xmlNode.Element("VB_Sell").Value, NumberStyles.AllowThousands);
            v_WorkRow["List"] = int.Parse(xmlNode.Element("VB_List").Value, NumberStyles.AllowThousands);
            v_WorkRow["Base"] = int.Parse(xmlNode.Element("VB_Base").Value, NumberStyles.AllowThousands);
            v_WorkRow["Count420_Vibration"] = int.Parse(xmlNode.Element("VB_420Count").Value, NumberStyles.AllowThousands);
            v_WorkRow["Count420_Keyphasor"] = int.Parse(xmlNode.Element("KP_420Count").Value, NumberStyles.AllowThousands);
            v_WorkRow["Count420_AxialVibrationProbe"] = int.Parse(xmlNode.Element("AX_420Count").Value, NumberStyles.AllowThousands);
            dt_Vibration.Rows.Add(v_WorkRow);
        }
    }
}
