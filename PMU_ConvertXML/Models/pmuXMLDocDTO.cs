﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMU_ConvertXML.Models
{
    public class pmuXMLDocDTO
    { 
        public int? PMUID { get; set; }
        public string XMLDoc { get; set; }
    }
}
