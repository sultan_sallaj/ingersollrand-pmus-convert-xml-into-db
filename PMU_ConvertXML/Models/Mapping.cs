//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMU_ConvertXML.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Mapping
    {
        public Nullable<int> ID { get; set; }
        public System.Guid keyPMUID { get; set; }
        public System.Guid fkAthenaProposalID { get; set; }
        public System.Guid fkTappCurveID { get; set; }
        public System.Guid fkPMUArchiveID { get; set; }
        public System.Guid fkUserID_Created { get; set; }
        public string pmuStrXMLUserDefined { get; set; }
        public string pmuStrXMLGeneralInformation { get; set; }
        public string pmuStrXMLInstrumentation { get; set; }
        public string pmuStrXMLPenaltyCommissions { get; set; }
        public string pmuStrXMLWriteIn { get; set; }
        public string pmuStrXMLSpareParts { get; set; }
        public string pmuStrXMLFieldService { get; set; }
        public string pmuStrXMLFreight { get; set; }
        public string pmuStrXMLCompressorHeaders { get; set; }
        public string pmuStrXMLCompressorOptions { get; set; }
        public string pmuStrXMLMotor { get; set; }
        public string pmuStrXMLStarter { get; set; }
        public string pmuStrXMLMotorChosen { get; set; }
        public string pmuStrXMLStarterChosen { get; set; }
        public string pmuStrXMLMotorOptions { get; set; }
        public string pmuStrXMLTotals { get; set; }
        public System.DateTime pmuDateCreated { get; set; }
        public string pmuStrXMLMiscellaneous { get; set; }
        public string pmuStrXMLPass { get; set; }
        public string pmuStrXMLProposal { get; set; }
    }
}
