//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PMU_ConvertXML.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Instrumentation_WaterManifold
    {
        public int ID { get; set; }
        public int HeaderID { get; set; }
        public Nullable<int> TemperatureLocal_TypeID { get; set; }
        public bool TemperatureLocal_Stage1_Inlet { get; set; }
        public bool TemperatureLocal_Stage1_Outlet { get; set; }
        public bool TemperatureLocal_Stage2_Inlet { get; set; }
        public bool TemperatureLocal_Stage2_Outlet { get; set; }
        public bool TemperatureLocal_Stage3_Inlet { get; set; }
        public bool TemperatureLocal_Stage3_Outlet { get; set; }
        public bool TemperatureLocal_Stage4_Inlet { get; set; }
        public bool TemperatureLocal_Stage4_Outlet { get; set; }
        public bool TemperatureLocal_ManifoldHeader_Inlet { get; set; }
        public bool TemperatureLocal_ManifoldHeader_Outlet { get; set; }
        public bool TemperatureLocal_OilCooler_Inlet { get; set; }
        public bool TemperatureLocal_OilCooler_Outlet { get; set; }
        public Nullable<int> TemperaturePanel_TypeID { get; set; }
        public bool TemperaturePanel_Stage1_Inlet { get; set; }
        public bool TemperaturePanel_Stage1_Outlet { get; set; }
        public bool TemperaturePanel_Stage2_Inlet { get; set; }
        public bool TemperaturePanel_Stage2_Outlet { get; set; }
        public bool TemperaturePanel_Stage3_Inlet { get; set; }
        public bool TemperaturePanel_Stage3_Outlet { get; set; }
        public bool TemperaturePanel_Stage4_Inlet { get; set; }
        public bool TemperaturePanel_Stage4_Outlet { get; set; }
        public bool TemperaturePanel_ManifoldHeader_Inlet { get; set; }
        public bool TemperaturePanel_ManifoldHeader_Outlet { get; set; }
        public bool TemperaturePanel_OilCooler_Inlet { get; set; }
        public bool TemperaturePanel_OilCooler_Outlet { get; set; }
        public Nullable<int> PressureLocal_TypeID { get; set; }
        public bool PressureLocal_Stage1_Inlet { get; set; }
        public bool PressureLocal_Stage1_Outlet { get; set; }
        public bool PressureLocal_Stage2_Inlet { get; set; }
        public bool PressureLocal_Stage2_Outlet { get; set; }
        public bool PressureLocal_Stage3_Inlet { get; set; }
        public bool PressureLocal_Stage3_Outlet { get; set; }
        public bool PressureLocal_Stage4_Inlet { get; set; }
        public bool PressureLocal_Stage4_Outlet { get; set; }
        public bool PressureLocal_ManifoldHeader_Inlet { get; set; }
        public bool PressureLocal_ManifoldHeader_Outlet { get; set; }
        public bool PressureLocal_OilCooler_Inlet { get; set; }
        public bool PressureLocal_OilCooler_Outlet { get; set; }
        public Nullable<int> PressurePanel_TypeID { get; set; }
        public bool PressurePanel_Stage1_Inlet { get; set; }
        public bool PressurePanel_Stage1_Outlet { get; set; }
        public bool PressurePanel_Stage2_Inlet { get; set; }
        public bool PressurePanel_Stage2_Outlet { get; set; }
        public bool PressurePanel_Stage3_Inlet { get; set; }
        public bool PressurePanel_Stage3_Outlet { get; set; }
        public bool PressurePanel_Stage4_Inlet { get; set; }
        public bool PressurePanel_Stage4_Outlet { get; set; }
        public bool PressurePanel_ManifoldHeader_Inlet { get; set; }
        public bool PressurePanel_ManifoldHeader_Outlet { get; set; }
        public bool PressurePanel_OilCooler_Inlet { get; set; }
        public bool PressurePanel_OilCooler_Outlet { get; set; }
        public int Sell { get; set; }
        public int List { get; set; }
        public int Base { get; set; }
        public int CountRTD { get; set; }
        public int Count420 { get; set; }
    }
}
