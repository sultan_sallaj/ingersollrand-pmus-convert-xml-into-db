﻿using PMU_ConvertXML.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PMU_ConvertXML.Helpers;

namespace PMU_ConvertXML
{
    public class pmuDTO
    {
        public int ID { get; set; }
        public Guid PMUID { get; set; }
        public DateTime DateCreated { get; set; }
    }

    public static class PMU_Operations
    {
        public static void mapNewPMUs(AthenaCPQEntities context)
        {
            IDictionary<int, Guid> newPMUs = _getNewPMUs(context);
            _legacyMapping(context, newPMUs);
        }

        private static IDictionary<int, Guid> _getNewPMUs(AthenaCPQEntities context)
        {
            Console.Write("-Fetching existing PMUs from new database");
            Console.WriteLine();
            IDictionary<int, Guid> newPMUs = new Dictionary<int, Guid>();
            newPMUs = (from newPMU in context.Headers.AsNoTracking()
                        select new { newPMU.ID, newPMU.PMUID }).ToDictionary(x => x.ID, y => y.PMUID);
            return newPMUs;
        }

        private static void _legacyMapping(AthenaCPQEntities context ,IDictionary<int, Guid> newPMUs)
        {
            DataTable dt = new DataTable();
            Console.Write("-Mapping old PMU GUIDs to new PMU IDs");
            Console.WriteLine();
            var oldPMUs = (from pmu in context.Mappings.AsNoTracking()
                            where !context.Headers.Any(x => (x.PMUID == pmu.keyPMUID))
                            select new pmuDTO
                            {
                                PMUID = pmu.keyPMUID,
                                DateCreated = pmu.pmuDateCreated
                            });

            dt = oldPMUs.CopyToDataTable<pmuDTO>();
            IDictionary<string, DataTable> sourceDT = new Dictionary<string, DataTable>();
            sourceDT.Add("PMU.Headers", dt);
            dump(sourceDT);
        }


        public static void dump(IDictionary<string, DataTable> sourceDT) {
            Console.Write("        Connecting to data layer of AthenaCPQ...");
            // Copy the DataTable to SQL Server
            using (SqlConnection dbConnection = new SqlConnection("Data Source=52.70.95.221,1433;Initial Catalog=AthenaCPQ;user id=Wise_Admin;password=W_Admin2;"))
            {
                dbConnection.Open();
                Console.Write("Connected!");
                Console.WriteLine();

                foreach (KeyValuePair<string, DataTable> table in sourceDT)
                {
                    Console.Write("        Adding " + table.Value.Rows.Count + " records to " + table.Key);
                    using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                    {
                        s.DestinationTableName = table.Key;
                        foreach (var column in table.Value.Columns)
                            s.ColumnMappings.Add(column.ToString(), column.ToString());
                        s.WriteToServer(table.Value);
                    }
                    Console.WriteLine();
                }
            }
        }

        public static void dump(string destTableName, DataTable sourceDT)
        {
            Console.Write("        Connecting to data layer of AthenaCPQ...");
            // Copy the DataTable to SQL Server
            using (SqlConnection dbConnection = new SqlConnection("Data Source=52.70.95.221,1433;Initial Catalog=AthenaCPQ;user id=Wise_Admin;password=W_Admin2;"))
            {
                dbConnection.Open();
                Console.Write("Connected!");
                Console.WriteLine();

                Console.Write("        Adding " + sourceDT.Rows.Count + " records to " + destTableName);
                using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                {
                    s.DestinationTableName = destTableName;
                    foreach (var column in sourceDT.Columns)
                        s.ColumnMappings.Add(column.ToString(), column.ToString());
                    s.WriteToServer(sourceDT);
                }
            }
            Console.WriteLine();
        }
    }
}
