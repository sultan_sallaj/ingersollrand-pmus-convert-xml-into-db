﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using PMU_ConvertXML.Models;
using System.Data.SqlClient;
using System.Data;

namespace PMU_ConvertXML
{
    public class XML_UserDefined
    {
        DataTable dt = new DataTable();
        DataColumn pmuGuidCol = new DataColumn();

        public XML_UserDefined ()
        {
            pmuGuidCol.DataType = typeof(Int32);
            pmuGuidCol.ColumnName = "PMUID";
            dt.Columns.Add(pmuGuidCol);
            dt.Columns.Add("DiscountType");
            dt.Columns.Add("AdditionalDiscount");
        }

        public void flatten()
        {
            Console.Write("Starting flattening of XML_UserDefined to PMUDiscounts table...");
            Console.WriteLine();
            Console.Write("    Connecting to Cameron_PMU.tblPMUs...");
            using (AthenaCPQEntities context = new AthenaCPQEntities())
            {
                Console.Write("Connected!");
                Console.WriteLine();
                Console.Write("    Parsing XML document and preparing data..");
                var oldPMUUserDefined = (from pmu in context.Mappings.AsNoTracking()
                                         where !context.Discounts.Any(x => (x.HeaderID == pmu.ID))
                                         select new pmuXMLDocDTO
                                         {
                                             PMUID = pmu.ID,
                                             XMLDoc = pmu.pmuStrXMLUserDefined
                                         });

                foreach (var details in oldPMUUserDefined)
                {
                    xmlMapping(details);
                }
            }
            Console.Write("Completed!");
            Console.WriteLine();
            Console.Write("    Connecting to AthenaCPQ.PMUDiscounts...");
            // Copy the DataTable to SQL Server

            IDictionary<string, DataTable> sourceDT = new Dictionary<string, DataTable>();
            sourceDT.Add("PMUDiscounts", dt);
            PMU_Operations.dump(sourceDT);

            Console.Write("Completed!");
        }

        private void xmlMapping(pmuXMLDocDTO details)
        {
            var xDoc = XDocument.Parse(new RijndaelContainer().Decrypt(details.XMLDoc));
            var userDefined = xDoc.Descendants("UserDefined").First();

            DataRow workRow = dt.NewRow();
            workRow["PMUID"] = details.PMUID;
            workRow["DiscountType"] = userDefined.Element("udefRadioChecked").Value.Split('_')[1]; ;
            workRow["AdditionalDiscount"] = string.IsNullOrEmpty(userDefined.Element("udefAdditionalDiscountValue").Value) ?
                                                (int?)null :
                                                Convert.ToInt32(userDefined.Element("udefAdditionalDiscountValue").Value); ;
            dt.Rows.Add(workRow);
        }
    }
}
