﻿using PMU_ConvertXML.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMU_ConvertXML
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("-Connecting to AthenaCPQ database...");
            using (AthenaCPQEntities context = new AthenaCPQEntities())
            {
                Console.Write("Connected!");
                Console.WriteLine();
                PMU_Operations.mapNewPMUs(context);

                // new XML_ShowConsoleOutput().flatten();
                // new XML_UserDefined().flatten();
                new XML_Instrumentation(context);

                Console.WriteLine();
                Console.Write("Press enter key to exit program!");
                Console.ReadLine();
            }
        }
    }
}
