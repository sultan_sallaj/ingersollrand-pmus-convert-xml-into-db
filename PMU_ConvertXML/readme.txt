﻿TODO:

	When adding new PMUs to the new PMUs (we need to add the user guid to that table - the one that created the pmu)



Note:

  Tried serializing XML into C# objects, but because all values are strings, conversion during serialization fails
  So I find it easier to create an XDocument, and go through each key node, and convert them manually that way.
  Changing the serializing code would have been more tedious. Such that an example of "False" and "True" does not
  create a boolean value. This type of value plagues the xml document. And yes, converting all these values do take a
  performance hit during migration. But at least they are being migrated!