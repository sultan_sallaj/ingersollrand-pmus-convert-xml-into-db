﻿using PMU_ConvertXML.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace PMU_ConvertXML
{
    public class XML_ShowConsoleOutput
    {
        public void flatten()
        {
            using (AthenaCPQEntities context = new AthenaCPQEntities())
            {
                Console.Write("Connected!");
                Console.WriteLine();
                Console.Write("Showing XML Documents for this table");
                var oldPMUXml = (from pmu in context.Mappings.AsNoTracking()
                                         select new pmuXMLDocDTO
                                         {
                                             PMUID = pmu.ID,
                                             //XMLDoc = pmu.pmuStrXMLUserDefined,
                                             XMLDoc = pmu.pmuStrXMLInstrumentation,

                                         }).Take(1);


                foreach (var details in oldPMUXml)
                {
                    Console.Write(new RijndaelContainer().Decrypt(details.XMLDoc));
                }
            }
        }
    }
}
